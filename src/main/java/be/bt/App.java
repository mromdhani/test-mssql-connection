package be.bt;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;

public class App {
    public static void main(String[] args) {
        System.out.println("Starting app ...");
        // Create a variable for the connection string.
        String connectionUrl = "jdbc:sqlserver://sql-datawh.admin.ulb.priv:1459;databaseName=ODS;user=infofepb_etl;password=eX3F99q5VfJ88eaD";

        try (Connection con = DriverManager.getConnection(connectionUrl); 
        		Statement stmt = con.createStatement();) {
        	
        	 ResultSet rs = con.createStatement().executeQuery("select * from [dbo].[PSAP_V_FAC2_CONTRATS_IFARAF]");
             
             ResultSetMetaData rsmd = rs.getMetaData();
             int cols = rsmd.getColumnCount();
             System.out.printf("The query fetched %d columns\n",cols);
             System.out.println("These columns are: ");
             for (int i=1;i<=cols;i++) {
                 String colName = rsmd.getColumnName(i);
                 String colType = rsmd.getColumnTypeName(i);
                 System.out.println(colName+" of type "+colType);
                  
             }
             while (rs.next()) {
                 System.out.println(" === This is a raw ....");
             }
     
            //Retrieving the data
           // ResultSet rs = stmt.executeQuery("select count(*) as nbre from ODS.dbo.PSAP_V_FAC2_ADRESSES_IFARAF");
            //rs.next();
            //Moving the cursor to the last row
            //System.out.println("Table contains "+  rs.getString("nbre")+  " rows");
        }
        // Handle any errors that may have occurred.
        catch (SQLException e) {
            e.printStackTrace();
        }
        System.out.println("End of the app ...");
    }
}
